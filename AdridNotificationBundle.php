<?php

namespace Adrid\NotificationBundle;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AdridNotificationBundle extends Bundle
{

    public function build(ContainerBuilder $container) {
        parent::build($container);

        $this->addRegisterMappingPass($container);
    }

    /**
     * @param ContainerBuilder $container
     */
    private function addRegisterMappingPass(ContainerBuilder $container)
    {
        $mappings = array(
            realpath(__DIR__. '/Resources/config/doctrine-model') => 'Adrid\\NotificationBundle\\Model',
        );

        $container->addCompilerPass(
            DoctrineOrmMappingsPass::createYamlMappingDriver($mappings)
        );

    }
}
