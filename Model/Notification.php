<?php
namespace Adrid\NotificationBundle\Model;

class Notification
{
    private $id;
    private $actor;
    private $recipient;
    /**
     * Short phrase identifying an action, e.g. created, deleted
     * @var string
     */
    private $verb;
    private $actionObject;

    /**
     * Notification constructor.
     * @param $actor
     * @param $recipient
     * @param $verb
     * @param $actionObject
     */
    public function __construct($actor, $recipient, $verb, $actionObject = null)
    {
        $this->actor = $actor;
        $this->recipient = $recipient;
        $this->verb = $verb;
        $this->actionObject = $actionObject;
    }

    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getActor()
    {
        return $this->actor;
    }

    /**
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * @return string
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * @return string
     */
    public function getActionObject()
    {
        return $this->actionObject;
    }
}
