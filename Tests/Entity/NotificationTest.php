<?php
namespace Adrid\NotificationBundle\Tests\Entity;

use Adrid\NotificationBundle\Tests\Fake\Model\Actor;
use Adrid\NotificationBundle\Tests\Fake\Model\ActionObject;
use Adrid\NotificationBundle\Model\Notification;

class NotificationTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var Actor
     */
    protected $actor;

    /**
     * @var Actor
     */
    protected $recipient;

    /**
     * @var string
     */
    protected $verb;

    protected $actionObject;

    /**
     * Sets up the fixture
     */
    protected function setUp() {
        $this->actor = new Actor();
        $this->recipient = new Actor();
        $this->verb = 'do something';
        $this->actionObject = new ActionObject();
    }

    public function testSimpleNotification() {
        $notification = new Notification($this->actor, $this->recipient, $this->verb);
        $this->simpleNotificationCheck($notification, $this->actor, $this->recipient, $this->verb);
    }

    private function simpleNotificationCheck(Notification $notification, $actor, $recipient, $verb) {
        $this->assertEquals($actor, $notification->getActor());
        $this->assertEquals($recipient, $notification->getRecipient());
        $this->assertEquals($verb, $notification->getVerb());
    }

    private function notificationWithActionObjectCheck(Notification $notification, $actor, $recipient, $verb, $actionObject) {
        $this->simpleNotificationCheck($notification, $actor, $recipient, $verb);
        $this->assertEquals($actionObject, $notification->getActionObject());

        $this->assertNotNull($notification->getActionObjectId());
        $this->assertNotNull($notification->getActionObjectClass());
    }

    public function testNotificationWithActionObject() {
        $notification = new Notification($this->actor, $this->recipient, $this->verb, $this->actionObject);
        $this->notificationWithActionObjectCheck($notification, $this->actor, $this->recipient, $this->verb, $this->actionObject);
    }
}