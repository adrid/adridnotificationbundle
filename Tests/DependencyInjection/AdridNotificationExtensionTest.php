<?php


namespace Adrid\NotificationBundle\Tests\DependencyInjection;


use Adrid\NotificationBundle\DependencyInjection\AdridNotificationExtension;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AdridNotificationExtensionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AdridNotificationExtension
     */
    private $extension;

    /**
     * @var ContainerBuilder
     */
    private $container;

    /**
     * Root name of the configuration
     * @var string
     */
    private $root;

    public function setUp() {
        $this->extension = $this->getExtension();
        $this->container = $this->getContainer();
        $this->root = 'adrid_notification';
    }


    /**
     * @expectedException Symfony\Component\Config\Definition\Exception\InvalidConfigurationException
     * @expectedExceptionMessage user_class
     */
    public function testConfigEmptyValues() {
        $this->extension->load(array(), $this->container);
    }

    private function getExtension()
    {
        return new AdridNotificationExtension();
    }

    private function getContainer() {
        return new ContainerBuilder();
    }
}
