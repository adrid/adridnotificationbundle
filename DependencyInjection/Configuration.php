<?php


namespace Adrid\NotificationBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $rootNode = $treeBuilder->root('adrid_notification');

        $rootNode
            ->children()
                ->scalarNode('user_class')->isRequired()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}