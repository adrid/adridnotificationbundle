<?php


namespace Adrid\NotificationBundle\Controller;


use Doctrine\ORM\EntityManager;
use Adrid\NotificationBundle\Model\Notification;

class NotificationController
{
    protected $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * Creates a notification and save it.
     * @param $actor
     * @param $recipient
     * @param $verb
     * @param null $actionObject
     */
    public function send($actor, $recipient, $verb, $actionObject = null) {
        $notification = new Notification($actor, $recipient, $verb, $actionObject);
        $this->em->persist($notification);
        $this->em->flush();
    }
}